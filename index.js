//naming convention for making classes (Capital letter)

class Student{
	// creating a constructor

	constructor(name, email, grades){
		this.name = name;
		this.email = email;
		this.grades = (grades.every(grade => (grade >= 0 && grade <= 100)) && grades.length === 4)? grades : undefined;
	}

	login() {
		console.log(`${this.email} is logged in`);
		return this;
	}

	logout(){
		console.log(`${this.email} is logged out`);
		return this;
	}
	listGrades(){
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`)
		return this;
	}

	computeAve(){
		    let sum = 0;
		    this.grades.forEach(grade => sum = sum + grade);
		    this.gradeAve = sum/4;
		    return this;
	}
	willPass() {
	    this.passed = this.computeAve() >= 85 ? true : false;
	    return this;
	}
	willPassWithHonors() {
		this.willPass();

	    if(this.passed){
	    	if(this.computeAve() >=90){
	    		this.passedWithHonors = true;
	    	} else {
	    		this.passedWithHonors = false;
	    	}
	    } else {
	    	this.passedWithHonors = undefined;
	    }

	    return this;
	}

}

let studentOne = new Student("John", "john@mail.com", [89, 84, 78, 88]);
let studentTwo = new Student("Joe", "joe@mail.com", [78, 82, 79, 85]);
let studentThree = new Student("Jane", "jane@mail.com", [87, 89, 91, 93]);
let studentFour = new Student("Jessie", "jessie@mail.com", [91, 89, 92, 93]);


// 1. What is the blueprint where objects are created from?

// Class
// 2. What is the naming convention applied to classes?
// Capital letter singular

// 3. What keyword do we use to create objects from a class?
// new

// 4. What is the technical term for creating an object from a class?
// instantiation

// 5. What class method dictates HOW objects will be created from that class?
// constructor

/*
	Function Coding 
	===
	1. Define a grades property in our Student class that will accept an array of 4 numbers ranging from 0 to 100 to be its value. If the argument used does not satisfy all the conditions given, this property will be set to undefined instead.

	2. Instantiate all four students from the previous session from our Student class. Use the same values as before for their names, emails, and grades.
*/

const gradeArr = [1, 100, 100, 50];
const gradeArr2 = [0, 1, 'aaa', 'sss', "ss"];
console.log(`creating test student with grade values of ${gradeArr})`);
let testStudent = new Student("test", "test@mail.com", gradeArr);
console.log(testStudent);

/*
	Quiz no 2

	1. Should class methods be included in the class constructor?
		no

	2. Can class methods be separated by commas?
		no

	3. Can we update an object’s properties via dot notation?
		yes

	4. What do you call the methods used to regulate access to an object’s properties?
		getter and setter

	5. What does a method need to return in order for it to be chainable?
		this

*/


/*

	Function coding:
	Modify the Student class to allow the willPass() and willPassWithHonors() methods to be chainable.
	Sample output:


*/
